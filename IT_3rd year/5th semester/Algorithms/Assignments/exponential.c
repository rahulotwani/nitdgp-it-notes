#include<stdio.h>
#define ll long long int
ll pw(ll a, ll b){
  ll r;
  if(b==0) return 1;
  r = pw(a,b/2);
  r = r*r;
  if(b%2) r = r*a;
  return r;
}
int main()
{
    ll a,n;
    scanf("%lld %lld",&a,&n);
    printf("%lld\n",pw(a,n));
}
