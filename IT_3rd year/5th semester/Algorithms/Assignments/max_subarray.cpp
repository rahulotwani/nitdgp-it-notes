#include<bits/stdc++.h>
#define ll long long int
using namespace std;
ll max(ll a,ll b,ll c)
{
    if(a>b)
    {
        if(a>c)
        return a;
        else return c;
    }
    else
    {
        if(b>c)
        return b;
        else return c;
    }
}
ll maxsum(ll* a,ll l,ll m,ll r)
{
    ll sum = 0,lsum = -1,rsum=-1,i;
    for(i=m;i>=l;i--)
    {
        sum=sum+a[i];
        if(sum>lsum)
        lsum = sum;
    }
    sum=0;
    for(i=m+1;i<=r;i++)
    {
        sum=sum+a[i];
        if(sum>rsum)
        rsum=sum;
    }
    return lsum+rsum;
}
ll maxsub(ll* a,ll l,ll r)
{
   if (l==r)
   return a[l];
   int m=(l+r)/2;
   return max(maxsub(a, l, m),maxsub(a, m+1, r),maxsum(a,l,m,r));
}
int main()
{
    ll n,a[100000],sum,i;
    scanf("%lld",&n);
    for(i=0;i<n;i++)
    scanf("%lld",&a[i]);
    sum = maxsub(a, 0, n-1);
    printf("%lld\n",sum);
    return 0;
}
