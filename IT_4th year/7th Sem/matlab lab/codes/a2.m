clc;
a = [10,0,20,11;12,7,9,20;0,14,16,18];
supply = [20;25;15];
demand = [10,15,15,20];
b = zeros(3,4);
row = 1;
col = 1;
cost = 0;
while(row <= 3 && col <=4)
    sub = min(supply(row), demand(col));
    supply(row) = supply(row)-sub;
    demand(col) = demand(col)-sub;
    b(row,col) = sub;
    cost = cost + a(row, col)*sub;
    if(supply(row)==0)
        row = row + 1;
    else
        col = col + 1;
    end
end
disp(cost);